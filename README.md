# ChipCaddy
This is the Team 16 project for ECE 445: Senior Design Laboratory in the Fall 2023 semester at the University of Illinois at Urbana-Champaign. 

## Problem:
The demand for trading card games is on the rise, but home poker games suffer from time-consuming chip handling. Manual counting, sorting, and distribution of chips waste valuable game time, leading to delays and potential errors.

## Solution:
A system combining sensors, motors, and a microcontroller is proposed. After each poker hand, chips are stacked and automatically sorted using color sensors, while the current pot value is displayed on an LCD screen. A reset button clears the pot, and in split-pot scenarios, a button shows chip denominations. This system aims to save time, streamline chip management, and enhance the overall experience of home poker games.

## Results:
[Video Demo](https://www.youtube.com/embed/CpfPaB9Krm0?si=x78G865Tmac2S20B)

[PowerPoint Summary](https://uillinoisedu-my.sharepoint.com/:p:/g/personal/jmwang5_illinois_edu/ETAItodV0qhFlyXcZGTBS2cBhk5qqKj15OOezn3-ILw3hA?e=DmLJ9v)

