# Worklog and Lab Notebook

<details>
<summary> Table of Contents </summary>

- [Week of 8/21](#week-of-821)
- [Week of 8/28](#week-of-828)
- [Week of 9/4](#week-of-94)
- [Week of 9/11](#week-of-911)
- [Week of 9/18](#week-of-918)
- [Week of 9/25](#week-of-925)
- [Week of 10/2](#week-of-102)
- [Week of 10/9](#week-of-109)
- [Week of 10/16](#week-of-1016)
- [Week of 10/23](#week-of-1023)
- [Week of 10/30](#week-of-1030)
- [Week of 11/6](#week-of-116)
- [Week of 11/13](#week-of-1113)
- [Week of 11/20](#week-of-1120)
- [Week of 11/27](#week-of-1127)

</details>



## Week of 8/21

**Objectives:** Form group, initiate discussion for project ideas, meet with TA. 

**Completed:** Posted on web board, formed group with Anish and Marvin, started initial brainstorm for project ideas.

First project brainstorm ideas:
- Barbell speed sensor

- Smart fridge camera

- Poker chip sorter and automatic shuffler

## Week of 8/28
**Objective:** Narrow down project ideas, write project proposal, begin looking at microcontrollers

**Completed:** Completed safety training and CAD assignment, got expreince working in KiCAD. 

Brainstorm:
- Smart fridge camera: 
Use camera module to detect when food is in fridge for too long, sends alert via UI interface (LED, LCD, app) to alert user food is going bad. This could be difficult because image detection is very power intensive, and a simple QR scanner would not work in the dark. The items would have to be scanned manually, taking alot of tine. _RFA submitted_

- Poker chip sorter: 
Use color sensor to detect color of chip, then use motor to push chip into neat stack for user to retreive. Display total count of pot onto LCD display. This could be very mechanically intensive, and may not be difficult enough for sensing aspect. _RFA submitted_

## Week of 9/4
**Objective:** Decide on final project idea, submit project idea and recieve approval. Start brinastorming parts, microcontroller, and initial design to machine shop.

**Completed:** Submitted project propsal for poker chip sorter. Preliminary design features a color sensor and two servo motors for sorting and counting chips. The design should be able to accomodate multiple colors and be able to count and sort them. Started thinking about design and began 3D cad simulation of what the device should maybe look like. 

![CAD Mockup](photos/445cad.png)


Project has recived approval. Next step is sourcing parts and beginning to work on official project proposal. Meeting with TA next week to dicuss potential obstacles, as well as things like budget and sourcing parts.

## Week of 9/11
**Objective:** Finish soldering assignment, meet with TA, submit project proposal, talk to machine shop, and submit team contract.

**Completed:** Began week with completing soldering assignment. Maybe think about using ATTiny chip? Soldering was difficult but was good expereince for future SMD soldering. Met with Nikhil - had initial discussion about project and potential obstacles. Got a good understanding of what will be required for the project. Started looking at parts for us to use:

- 12V battery: https://www.amazon.com/Tenergy-Capacity-Rechargeable-Replacement-Equipments/dp/B077Y9HNTF

- Servo motor: https://www.google.com/search?client=safari&sca_esv=588456036&rls=en&sxsrf=AM9HkKlPFsiFruDWo79i0R5eeKzGJs3acw:1701891829492&q=12v+battery+pack&tbm=isch&source=lnms&sa=X&ved=2ahUKEwjepcWPyfuCAxWOjIkEHeDfBLkQ0pQJegQIDBAB&biw=756&bih=790&dpr=2#imgrc=JQ9aCojKvrtIdM

- LCD: https://www.crystalfontz.com/product/cfah1602btmijt-16x2-character-lcd?kw=&origin=pla&gclid=CjwKCAiA1MCrBhAoEiwAC2d64WdoFSVJbh3a3Nw-Gy8LADagUFiAVgyfVHi5mvEBgeUCj0sKeHZp6hoCJVcQAvD_BwE

Completed project proposal, drew up basic sketch of design.

![Sketch Mockup](photos/445mockup.png)


 Drew up first block diagram, and subsystem overview. 

 ![Block](photos/445initialblock.png)


### Power Subsystem
- **Power Source**: 12V battery.
- **Function**: Provides stable, regulated voltage to system components.
- **Components Powered**: RGB color sensor, servo motors, LCD display, ESP32 Microcontroller.

### Control Subsystem
- **Core Component**: ESP32 Microcontroller.
- **Functions**: Processes color sensor data, pot counter calculations, button interactions, controls LCD display and motors.
- **Role**: Central control unit for the chip counter system.

### Motor Subsystem
- **Function**: Manages physical movement of chips (sorting and dispensing).
- **Communication**: Serial with control unit.
- **Instructions**: Placement, organization, and dispensing quantity of chips.

### Sensing Subsystem
- **Components**: RGB color sensor at hopper base.
- **Function**: Detects chip value by measuring reflected light wavelengths.
- **Process**: Communicates chip value to control unit for sorting.

### User Interface Subsystem
- **Components**: LCD display, buttons.
- **Display Function**: Shows real-time pot value.
- **Button Function**: Informs microcontroller about split pots, adjusts pot distribution.


## Week of 9/18
**Objective:** Keep sourcing materials, start looking at PCB design materials, brainstorming design and getting ready for design doc due next week.

**Completed:**
- Started initial PCB design in KiCAD, focusing on layout for control subsystem.
- Researched PCB design tools and tricks.
- Began drafting the design document, outlining the functional requirements and technical specifications of our poker chip sorter.
- Held a team meeting to discuss the layout of the user interface, specifically the LCD display and button placement.
- Completed a rough sketch of the device's exterior design and in Fusion360.

**Design Considerations:**
- **PCB layout:** Prioritizing compact design while ensuring all components fit and are easily accessible.
- **User Interface:** Ensuring the LCD is visible and buttons are ergonomically placed for ease of use.
- **Motor Placement:** Strategizing motor placement for efficient chip sorting and dispensing, as well as avoiding any wire entanglement.
- **Battery Integration:** Designing a safe and accessible compartment for the 12V battery.

**Challenges:**
- **PCB complexity:** Ensuring all connections are correct and minimizing the risk of short circuits.
- **Aesthetic:** Balancing functionality with an appealing design for potential users.
- **Ergonomics:** Designing the interface to be user-friendly and intuitive.

**Next Steps:**
- Finalize PCB design.
- Complete and submit the design document with detailed specifications and sketches.
- Start working on the code for the ESP32 microcontroller.

## Week of 9/25
**Objective:** Submit design document, order inital parts for testing, begin programming on development board, prepare for design review.

**Completed:** Submitted design document, we decided on 4 main high level requirements.

### High Level Requirements

For the project to be considered successful, it must fulfill the following requirements:

#### 1. Rapid Counting
- The device should append the count within 5 seconds of a chip being read by the color sensor.

#### 2. Reset Functionality
- After ejecting all chips, the winner should be able to reset the pot count to 0.

#### 3. Handling Split or Chop Pots
- Users must be able to manually select the number of ways the pot will be split.
- The LCD display should show the respective color denominations for the largest division:
  - White: $0.50
  - Red: $0.25
  - Green: $2
  - Blue: $1

#### 4. Tally and Verification
- The device will maintain a tally of the total number of chips and the count for each color.
- It should verify that the sum of each colored chip matches the total chip count.
- The total number of chips inserted should be displayed on the LCD.

During the design phase, we explored several  features. Key among these was wireless connectivity (e.g., Bluetooth or Wi-Fi) for remote operation. However, this was excluded due to its potential to increase system complexity and cost, possibly hindering the project's feasibility.

We also contemplated a more sophisticated user interface, such as a touchscreen. This idea was eventually set aside in favor of a simpler, more cost-effective LCD and button interface, aligning better with our user-friendly design.

Another notable consideration was a more complex sorting mechanism to accommodate varying chip sizes and shapes. This idea was not pursued further. Our target audience predominantly uses standard chip sizes, and the additional mechanical complexity could introduce unnecessary calibration and maintenance challenges.

## Week of 10/2
**Objective:** Complete design review, make necessary changes to block diagram and design, have discussion with machine shop and start testing intitial components.

**Completed:** Had our design review - got a lot of positive feedback. Few big pivots in design: transition from ESP32 to STM32. This decision weas based on not requiring alot of the wireless connectivity features of the ESP32, as well as wanting to pursue a different path than most groups. The STM32's robust architecture and advanced features, such as better interrupt handling and increased memory capacity, make it more suitable for the complex tasks required by our device. Secondly, we pivoted from a 12V battery pack input to a 9V input. We do not need 12V for any part of our design, so would be simpler just to incorporate 9V. The power consumption analysis below shows the disparity in dropout for our voltage regulator. When a voltage regulator steps down a significantly higher input voltage to a lower operating voltage, it can lead to increased heat dissipation and reduced energy efficiency. This is because the excess voltage is essentially 'burned off' as heat, which not only wastes power but also can stress the electronic components, potentially reducing their lifespan. By using a 9V input, closer to our system's operational voltage requirements, we minimize these losses, enhancing the overall energy efficiency and reliability of our device. This approach aligns better with our design philosophy of creating a more streamlined, efficient, and sustainable product. 

 ![Voltage Dropout](photos/445voltagedrop.jpeg)

We modified our block diagram accordingly to reflect these new changes. We also settled on our parts - we are going to use the base servo provided by the card shuffling group from last semester and a linear actuator to push out the chips. 

- Servo base 
- Linear actuator
- LCD
- Buttons
- TCS3200 color sensor

## Week of 10/9
**Objective:** Begin testing LCD, interfacing with TCS3200, complete teamwork eval.

**Completed:** Adjusted block diagram again, for similar reasons to above chose 6V battery pack. This works well with our components because we are using 6V servo motor and 6V linear actuator so will be simple to simply power with this pack. NiMH battery pack was chosen due to stability and simplicity to be recharged.  

 ![Block](photos/445finalblock.png)

Was able to interface with LCD display with Nucleo-64 and CubeIDE to make the display light up and display text. Utilized multimeter to ensure all pins on display were stable and recived expected 3.3V. May pivot from CubeIDE to Ardiuno due to simplicity - Cube is difficult to understand without solid background in embedded. 

Completed PCB schematic - was fairly simple to port over existing example board with STM32 to our design. Our design features only the bare bones required for STM32 programming, such as SWD pins, and GPIO pins for our peripherals and motors. 

![PCB Schematic](pcb/pcbschematic.png)

Working on layout next week, as well having final discussions with machine shop. We missed initial PCB deadline but we are close to having final PCB design ready to go. Should be good to send out next week.

## Week of 10/16
**Objective:** Finish PCB layout, send in order. Continue interfacing with peripherals, begin testing TCS3200 color sensor. 

**Completed:** Finsihed laying out the PCB. Was important to maintain thicker trace width (10 mils) for power traces to ensure provided current will be able to flow. We ran into some issues with traces but were solved by adding some ground stitching around the affected areas. 

![PCB Schematic](pcb/pcblayout.png)

The gerber file was generated and testing on PCB should be completed as soon as we recieve the physical board. 

TCS3200 was intiially brought up on the Ardiuno. This tutorial was followed on the Ardiuno Uno board initally and then the STM32 Nucleo Board.
https://randomnerdtutorials.com/arduino-color-sensor-tcs230-tcs3200/

Was able to interface with both the LCD and color sensor simultaneouosly. The received output frequencies for each color were tabulated and recorded for calibration. 

| Trial | Color | Red (R) | Green (G) | Blue (B) |
|-------|-------|---------|-----------|----------|
| 1     | Red   | 14      | 37        | 23       |
| 2     | Red   | 16      | 36        | 27       |
| 3     | Red   | 15      | 33        | 26       |
| 4     | Red   | 13      | 35        | 24       |
| 5     | Red   | 17      | 34        | 25       |
| 1     | Green | 29      | 26        | 22       |
| 2     | Green | 30      | 22        | 21       |
| 3     | Green | 31      | 25        | 19       |
| 4     | Green | 28      | 27        | 18       |
| 5     | Green | 32      | 23        | 20       |
| 1     | Blue  | 34      | 30        | 18       |
| 2     | Blue  | 33      | 28        | 21       |
| 3     | Blue  | 35      | 29        | 19       |
| 4     | Blue  | 31      | 31        | 20       |
| 5     | Blue  | 32      | 27        | 22       |
| 1     | White | 11      | 8         | 4        |
| 2     | White | 10      | 12        | 6        |
| 3     | White | 9       | 11        | 5        |
| 4     | White | 8       | 9         | 7        |
| 5     | White | 12      | 10        | 3        |

These tabulated values will be very imporant in determining the range of values for our program. Below are the functions that are used to determine the color of the chip. 

```cpp
bool isInRange(int value, int target, int tolerance) {
  return value >= (target - tolerance) && value <= (target + tolerance);
}

String determineColor(int red, int green, int blue) {
  const int tolerance = 3; 
  
  if ((isInRange(red, 15, tolerance) && 
       isInRange(green, 35, tolerance) && 
       isInRange(blue, 25, tolerance))) {
    return "Red";
  } else if (isInRange(red, 30, tolerance) && 
             isInRange(green, 24, tolerance) && 
             isInRange(blue, 20, tolerance)) {
    return "Green";
  } else if (isInRange(red, 33, tolerance) && 
             isInRange(green, 29, tolerance) && 
             isInRange(blue, 17, tolerance)) {
    return "Blue";
  } else if (isInRange(red, 10, tolerance) && 
             isInRange(green, 10, tolerance) && 
             isInRange(blue, 5, tolerance)) {
    return "White";
  } else {
    return "Unknown";
  }
}
```

As is evident from the code, we set these tolerance values from the table for maximum efficacty of chip detection. 

## Week of 10/23
**Objective:** Copmleted IPR, perform testing on peripheral pins on PCB, begin attempting to program PCB, integrate motors and buttons into device on Nucelo board.

**Completed:** Submitted my IPR. Performed initial voltage testing on PCB after SMD soldering and using oven.

| Component       | Voltage Reading 1 | Voltage Reading 2 | Voltage Reading 3 |
|-----------------|-------------------|-------------------|-------------------|
| Servo Motor     | 6.05V             | 5.95V             | 6.08V             |
| Linear Actuator | 6.02V             | 6.09V             | 5.98V             |
| LCD             | 3.28V             | 3.31V             | 3.29V             |
| MCU             | 3.32V             | 3.30V             | 3.33V             |

Our voltage readings are consistent with what we expected from our power subsystem. This verifies that our power subsystem is functional.

Also started integration of motors on Nucleo board. Servo motor was connceted to board and tested to see if would rotate. We were able to rotate the motor as well as control set angles. This will be useful when we control the motor to direct to a certain bin. Here is quick outline of testing procedure to verify system integration:

Place chip in front of sensor. Sensor reads color, MCU on dev board makes conclusion about what color is detected. LCD will then display the color on screen. Motor will then spin to that specific angle. We verified this and confirmed that these subsystems are functional. 

We did run into an issue with the linear actuator. Will need to perform additional testing to verify if actuator or MCU issue.

## Week of 10/30
**Objective:** Continue integrating subsystems, testing motors, and ensuring full system functionality.

**Completed:** Added buttons to system on breadboard, using serial monitor to test if dtected by MCU. See that it is working correctly - used https://docs.arduino.cc/built-in-examples/digital/Button for documentation. 

```cpp

if (digitalRead(RST_BUTTON) == LOW) {
  delay(50); 
  
  if (digitalRead(RST_BUTTON) == LOW) { 
    lcd.clear();
  SplitButtonCount = 0;
    // Calculate the total number of chips
    int totalChips = redCount + blueCount + greenCount + whiteCount;
    // Display the total number of chips
    lcd.print("Total Chips: ");
    lcd.print(totalChips);
    // Wait a bit for the message to be read
    delay(2000);
    currentPot = 0.00;
    redCount = 0;
    blueCount = 0;
    greenCount = 0;
    whiteCount = 0;
    splitButtonPressCount = 0;
    // Clear the screen and print the pot value
    lcd.clear();
    lcd.print("   Pot: $");
    lcd.print(currentPot, 2); 
    
```

Above is the logic for the button press of split pot. We utilized a momentary button and added a slight delay when the button is pressed for debouncing. The functionality was fully verified on the serial monitor and on the LCD. 

The linear actuator was still not functioning, but was working in the machine shop with the servo test device. Need to continuie debugging, could be issue with current draw. 

![Color Spectrum](photos/445colorspectrum.png)

Had a slight issue with the color determination for green and blue, above is photo from datasheet. Spectral responsitivity for green and blue are very similar, resulting in siginficant overlap. This means is sometimes hard to differentiate between the two. This was tempraroly sovled by reducing tolerance range to only 2, but may cause issues in final demo. 


## Week of 11/6
**Objective:** Resolve issue with linear actuator, finalize design with machine shop, debug PCB.

**Completed:** Solved issue with linear actuator, issue was current draw. Tested with a multimeter and adjuste the voltage supply it was recieving. Performed system integration testing with linear actuator. 

```cpp

void loop() {  
  delay(2000);
  int redValue = readColor(LOW, LOW); // Reading Red filter (Red light)
  int greenValue = readColor(HIGH, HIGH); // Reading Green filter (Green light)
  int blueValue = readColor(LOW, HIGH); // Reading Blue filter (Blue light)

  String color = determineColor(redValue, greenValue, blueValue);
  Serial.println(color);
  if (color == "Red") {
    myservo.write(servoPositionRed);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += RED_VALUE;
    redCount += 1;
  } else if (color == "Green") {
    myservo.write(servoPositionGreen);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += GREEN_VALUE;
    greenCount += 1;
  } else if (color == "Blue") {
    myservo.write(servoPositionBlue);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += BLUE_VALUE;
    blueCount += 1;
  } else if (color == "White") {
    myservo.write(servoPositionWhite);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += WHITE_VALUE;
    whiteCount += 1;
  } else {
 myservo.write(servoStartPosition);
  }
  
```

Above is code for motor movement. As color is detected, servo will rotate to specific bin and wait for linear actuator to fully retract. Device will then reset and accept next color input. 

## Week of 11/13
**Objective:** Retreive device from machine shop, test and give back for any final design alertations. Get system working on final device, fix PCB. 

**Completed:** PCB debugging. Verified schematic for pull-up resistors, voltage, current draw. Everything looks good. Need to debug with TA asap before final demo. 

Device was retrieved from machine shop. Needed to rotate servo motor, as the DOF was angled to the incorrect side. Final device is larger than expected, resulting in slightly heavier load for motor. After wiring, servo motor has slight difficulties with reaching red bin. This is due to stall torque not being as high as expected with our 5V input from Nucleo board. 

![Device](photos/445device.HEIC)


## Week of 11/20
Fall break, no progress made.

## Week of 11/27
**Objective:** Complete calibration, machine shop aleterations, and wiring of all peripherals onto Nucleo-64 board. Ensure proper functionality of device in all scenarios. Debug PCB and test for functionality.

**Completed:** Finsihed calibrating color frequencies with TCS3200 and STM32. Observed odd behavior of serial monitor readings - sometimes frequency would jump and skew calibration. Fixed with even tigher tolerance.

Linear actuator gets stuck on PVC tube. Suspect due to slight incosistencies in chip thickness and stroke angle - fixed with slightly elevating linear actuator so stroke would be angle downward slightly. 

![Lin Act](photos/445linact.png)

Finished wiring and attatching peripherals to device, machine shop provided plastic enclosures for both UI subsystem (LCD, buttons) and control subsystem (STM32). Used jumper wires to extend from enclosure to peripherals and motors, making sure to keep long to not get snagged on any of the moving parts. 

Debugged PCB with help of Jason. STM32 was soldered on incorrectly. Using hot air was able to remove IC and resolder correctly. Device is not being detected by ST-Link and is able to be properly flashed. Pin assigmnets in STMduino were changed to match with those on IC. Program runs as intended on PCB and breadboard - timing is slightly different so needed to adjust from 250 ms delay on nucleboard to only 150 ms on IC. Ready for final demo.

**Final demo went well! Was able to display full functionality on demo board, as well as partial system integration on PCB. Device had 100% success rate with determining color chip. **


