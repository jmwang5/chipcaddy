# Worklog and Lab Notebook

<details>
<summary> Table of Contents </summary>

- [Week of 8/21](#week-of-821)
- [Week of 8/28](#week-of-828)
- [Week of 9/4](#week-of-94)
- [Week of 9/11](#week-of-911)
- [Week of 9/18](#week-of-918)
- [Week of 9/25](#week-of-925)
- [Week of 10/2](#week-of-102)
- [Week of 10/9](#week-of-109)
- [Week of 10/16](#week-of-1016)
- [Week of 10/23](#week-of-1023)
- [Week of 10/30](#week-of-1030)
- [Week of 11/6](#week-of-116)
- [Week of 11/13](#week-of-1113)
- [Week of 11/27](#week-of-1127)
- [Week of 12/4](#week-of-124)

</details>



## Week of 8/21
**Objectives:** 
1. Form A Group of Three
2. Brainstorm Ideas for Web Board due Thurs
3. Finish Web Board Post to Take Ownership of Idea

**Completed Tasks**:
- Formed Three-Person group with Marvin and Justin
- Each member posted an idea to the Web Board

*Justin's Post:* Smart Fridge, Food Spoilage Prevention System

**Details:** 
- The device is equipped with one or more camera module(s) capable of taking high resolution images of the food items.
- Microcontroller will be used for image processing and user-interfacing
- User interface will have touchscreen, LCD, buttons, and/or speaker to communicate with user

**Feedback:**
- Should the device analyze the visual appearance of food or expiration dates?
- Dataset to train a camera on what spoiled food looks like might be difficult to find
- Does using the device give the user an incentive other than saving food?

*Marvin's Post:* Barbell Path/Movement Analyzer

**Details:**
- Integrated sensors with proximity or ultrasonic features to analyze barbell Path
-Microcontroller to process sensor input and advise user
- Speaker or user interface module to communicate and advise user

**Feedback:**
- Might not be able to withstand wear and tear
- Gyms would not buy
- A path analyzer may not be able to give adequate advice on technique

*Anish's Post:* Poker Chip Pot Counter and Card Dealer

**Details:** 
- Dealer Apparatus would be mechanically abled to hold a deck of cards and spit them out face down at angles respective of the number of players
- PVC Pipe apparatus for players to insert their chip bets into
- Weight or Color Sensor to count the monetary value of the Pot
- Microcontroller or FPGA will handle logic of counting pot and displaying info to user
- Powered by external battery and uses LCD to display relevant information to user

**Feedback:**
- Not a real problem
- How will cards remain face down?
- Refer to Spring 2023 Project 24, as it is similar

## Week of 8/28
**Objectives:** 
1. Refine most promising Idea from Web Board
2. Get out an RFA by End of Week, preferable by Extra Credit Deadline

**Completed Tasks**:
- Submitted RFA, but got rejected
- Need to Approach Jason at Office Hours for more help on how to make design viable

RFA details:
- Display: LCD that shows pot count
- Color Sensor, attached at a location in which only one chip will be analyzed at a time, to get the color of the chip
- Servos for dispensing and rotating base for organizing chips chromatically
- Button for allowing User interfacing
- MCU for controlling logic of pot counting and splitting pot

## Week of 9/4
**Objectives:** 
1. Speak to Jason on Tuesday office hours, figure out how to tweak Poker Chip Idea for approval
2. Get RFA approved by Thursday Deadline

**Completed Tasks**:
- Project approved

Feedback from Jason:
- Device that accommodates pot counting via color sensor analysis and sorting is complex enough for project approval
- Completely eliminate card dealing aspect

## Week of 9/11
**Objectives:** 
1. Meet with TA, Nikhil Arora
2. Get Proposal and Team Contract done ASAP

**Completed Tasks**:
- Met With Nikhil, He says he is the "resident gambler"
- TA is confident we will be able to complete the project if we stay on top of it
- Completed Proposal, Team Contract, and met with Machine Shop

**1. Proposal**
Details:
- Problem: No Home Poker Game solution exists for counting chips and displaying pot count as shown on Poker Streams on Live Television

- Solution: Device uses sensor, motors, internal logic to sort chips and display/split pot count. Goal is to mimic the speed of online poker, as much as possible

- High Level Requirements:
1. Device appends count within 6 seconds of chips being inserted
2. Device accommodates pot reset using a buttons
3. User can chop the pot multiple ways with a button

- Visual Aid:
![Visual Aid](photos/445mockup.png)

- Initial Block Diagram:
![Initial Block Diagram](photos/445initialblock.png)

- Subsystems:
1. Power Subsystem: 12V Battery + 3.3 Linear Voltage Regulator
2. Sensing Subsystem: RGB Color Sensor
3. Control Subsystem: ESP32 MCU
4. Motor Subsystem: Servos for Sorting and Dispensing, powered by MCU
5. UI Subsystem: LCD, and Two Buttons for Split and Resets 

- Tolerance Analysis:
Want to minimize friction coefficient as much as possible to promote the efficiency and speed of our design: *f = μN*

**2. Team Contract:**

Main Points:
- Maintain Strong Communication
- If one teammate is busy one week, other teammates will pick up and that particular teammate will compensate the next week
- Meet every Tuesday when lecture would have normally happened, and meet on Monday at 4:15 PM for TA Meetings

**3. Meeting with Gregg @ Machine Shop:**

- Gregg Mentioned a hopper/funnel apparatus for directing chips into pipe might be a problem for chips getting jammed or stuck

- Provided Gregg with Justin's CAD Rendering:
![Initial Block Diagram](photos/445cad.png)

- Mentioned a linear actuator could be used to push chips out of bottom of pipe, he conveniently had one to show us and said we could use it.

- Showed us Project 24 from Spring 2023, and said we can build our design on top of their rotational base

## Week of 9/18
**Objectives:** 
- Order Parts, Look at PCB Design Guidelines, Meet with Nikhil

**Completed Tasks**:
- Met With Nikhil
- Ordered Parts
- Began Looking at PCB Design Guidelines

**Parts List:**

| Description   | Manufacturer         | Quantity | Cost ($)   | 
| ------------  | -------------------- | -------- | ---------- |
| LT1117CST-3.3 | Linear Technology    | 1x       | 6.30       |          
| HS-318 Servo  | HiTec                | 1x       | 11.99      |           
| STM32F103C8T6 | STMicroelectronics   | 1x       | 6.42       |
| TCS3200       | DFRobot              | 1x       | 7.90       |
| LCD           | Newhaven Display     | 1x       | 13.00      |
| 10u Cap       | Kemet                | 1x       | 0.24       |
| 22u Cap       | Cal-Chip Electronics | 1x       | 0.40       |
| 6V Battery    | Tenergy Corporation  | 1x       | 11.50      |
| 1u Cap        | Kemet                | 2x       | 0.20       | 
| NUCLEO-F103RB | STMicroelectronics   | 1x       | 10.77      |
| PCB           | PCBWay               | 10x      | 21.69      |
| Total Cost:   |                      |          | 90.41      |

**Meeting with Nikhil:**

- Considering using STM32 as opposed to ESP32, no need for extensive functionality ESP32 offers.
- Moved away from 12V battery, and singular regulator. MCU cannot provide sufficient power for the motors. Motors need 6V, MCU can only supply around 3.3.
- Weighed the pros and cons of using 9V and two regulators instead, one that steps down to 3.3V and another that steps down to 6V.

- LTSPice Simulation on Configuration of Regulator:
![LTSpice](photos/Picture1.png)

- Eventually ended up using a 6V supply with one regulator, this made the most sense.
- Tolerance Analysis from Proposal needs to be more electrical based rather than mechanical. Consider doing thermal analysis of regulator instead.



**Prepare for PCB Review**:

*Design Considerations:*

- **PCB Design:** Start looking at what needs to be done for the schematic. Schematic should be fairly simple. Datasheets of the parts have details on how to configure the circuitry for various parts. 

- **PCB layout:** Want to ensure a compact design, ensured that small things such as power tracks being thicker, and sharp-angled tracks were eliminated. This way we won't have to send out more than one order.

## Week of 9/25
**Objectives:** 
- Finish Design Document, Begin Programming on Development Board

**Completed Tasks**:
- Got the LCD to Display
- Submitted Design Document

**Design Doc:**

**Final High Level Requirements**: 

1. The device should append the count within 5 seconds of the chip being read by the color sensor. 

2. Upon ejecting all chips from our contraption, the winner of the pot will be able to reset the pot count to 0.
3. In the case of split or chop pots the user will be able to manually choose the number of ways the pot will be split, and the respective color denominations (White- $0.50, Red- $0.25, Green- $2, Blue- $1) for the largest division will be shown on the LCD display.

4. The device will keep a tally of the number of chips counted, and the number of chips counted for each color. It will then ensure the sum of the number of each colored chip matches the total number of chips and display the total number of chips inserted on the display. 

**Thermal Analysis:**

Tjunction = Tambient  + Pdissip × RΘ = 30℃ + ((Vin - Vout)  × Iload) × 15℃ = 30℃ + ((6 V - 3.3 V)  × 43 mA) × 15℃ = 31.741℃ < 150℃. 

The Regulator is Okay for Use.

**LCD Functionality:**

- After failing to use the STM32Cube IDE, the traditionally used IDE for STM Embedded Applications, we pivoted to using a legacy version of the Arduino IDE that is compatible with the STMduino extension. This extension allowed us to easily port the code on the Nucleo 64 Development Board

- This was very convenient, because there was a plethora of code online for using Arduino with peripherals like buttons and LCDs

- We were able to get the LCD to display a message fairly easily.

## Week of 10/2
**Objectives:** 
- Finish Design Review, and attend PCB Review

**Completed Tasks**:
- Finished Design Review
- Attended PCB Review, made tweaks to current version of the schematic

**Feedback from Design Review:**

- Need to make R & V Tables more specific, e.g. if we need to see the Pot Count Reset on the LCD, how fast should this change be noticed, 1 second, 2 seconds?
- Set the bar low for high level requirements, but at the same time be specific. Set the color denominations to: White- $0.50, Red- $0.25, Green- $2, Blue- $1. 

**PCB Review:**

- We already had some schematics from our design document based on the datasheet. Below is a picture of our UI subsystem schematic connections:

![UI](photos/Picture2.png)

- Questions on how to get footprints if they are not already on KiCad, Jason suggested UltraLibrarian and SnapEDA

- Jason also suggested, using the example STM32 board on the course wiki, as it had most of our connections along with the correct footprints: 

![Ex](photos/schematicEX.png)

## Week of 10/9
**Objectives:** 
- Finish PCB Schematic, Make Final Changes to Design.

**Completed Tasks**:
- Finished PCB Schematic by adding our modules to Jason's Example from the Course Wiki
- Make Final Design Considerations

**Finished PCB Schematic:**

- After using Jason's example schematic we made final adjustments, and imported footprints for the modules that we added:

![PCB Schematic](pcb/pcbschematic.png)

- After Speaking with Nikhil, there was a concern on whether we needed an H-Bridge or not. We eventually decided not to use one because we thought it would be easier. At this point we wanted to design our device as simple as possible, while still being able to reach all our High Level Reqs.

- Our Final Block Diagram, after making changes to the Power Supply method, and after noting that our motor has an internal driver: 

![Block](photos/445finalblock.png)

## Week of 10/16
**Objective:** 
- Finish PCB layout and send in order with second PCBWay wave. Solve any outstanding DRC's. 

**Completed:** 
- Finished PCB layout
- Solved DRC's
- Made 2nd Wave of PCBWay orders and passed audit

![PCB Schematic](pcb/pcblayout.png)

We had some problems with DRC's specifically **clearance** and **air nets**. We attempted by using an autorouting AI model online to assist us with routing. After using this the first time, we were left with almost 150 DRC's. We then attempted routing manually and only had about 10-12 DRC's.

We attended office hours, and a TA was able to help us solve the DRC's by adding stitching vias and ground planes. The proximity DRC's were solved simply by rearranging the nets. We also made sure to eliminate any sharp-angled tracks and make all the power tracks 1.00 mils. 

## Week of 10/23
**Objective:** 
- Finish IPR's and begin interfacing TCS3200 color sensor with Development Board

**Completed:** 
- Read in TCS3200 colors as frequencies in Arduino Serial Monitor, and mapped them to integer values
- Finished IPR detailing how I was going to program this

Used Documentation Found At: https://randomnerdtutorials.com/arduino-color-sensor-tcs230-tcs3200/

The tutorial found at this website was helpful in deriving frequencies for reading the chip colors at close proximity. By defining the same pins, connected to the arduino as GPIO pins on the Nucleo Board we were able to run the same code on the development board. A code snippet for differentiating the colors using integer tolerances is shown below:

```cpp
bool isInRange(int value, int target, int tolerance) {
  return value >= (target - tolerance) && value <= (target + tolerance);
}

String determineColor(int red, int green, int blue) {
  const int tolerance = 3; 
  
  if ((isInRange(red, 15, tolerance) && 
       isInRange(green, 35, tolerance) && 
       isInRange(blue, 25, tolerance))) {
    return "Red";
  } else if (isInRange(red, 30, tolerance) && 
             isInRange(green, 24, tolerance) && 
             isInRange(blue, 20, tolerance)) {
    return "Green";
  } else if (isInRange(red, 33, tolerance) && 
             isInRange(green, 29, tolerance) && 
             isInRange(blue, 17, tolerance)) {
    return "Blue";
  } else if (isInRange(red, 10, tolerance) && 
             isInRange(green, 10, tolerance) && 
             isInRange(blue, 5, tolerance)) {
    return "White";
  } else {
    return "Unknown";
  }
}
```
Once we were able to differentiate the colors, we simply had to extend the code to assign monetary values to the colors.

There was an issue differentiating between the blue and green chips, these circumstances also happened to change depending on where we did our calibrations. The reason for the struggle with differentiating green and blue is their frequency overlap as shown in the picture below. We figured we would save this issue for after we got our contraption from the machine shop, because we would have to recalibrate to those lighting conditions anyways. 

![Color Spectrum](photos/445colorspectrum.png)

## Week of 10/30
**Objective:** 
- Soldered parts onto PCB, ensured all components were receiving power. Used Oven to Solder MCU and smaller parts.

**Completed:** 
- Soldered PCB, ensured that all parts received necessary power.

We got experience using the Reflow Oven and soldered our PCB using the stencil that came with our PCB. I struggled to keep the stencil flush on the PCB at first, and this caused an excess of paste on the pads. I eventually got it just right, and got rid of any bridging on the STM32 pins using flux.

Once the PCB was soldered, we tested the power on all ends of our power traces for components designated to receive 6V as well as 3.3V. All components were powered such that they fulfilled the requirements of our power subsystem R & V's.


## Week of 11/6
**Objective:** 
-Finish Integrating Subsystems, get motors functional, actuator functional, and buttons for Mock Demo.

**Completed:**
- Got Motors to Rotate
- Fixed Issues with actuator
- Configured Code for Buttons on Breadboard

Got motors to rotate to hypothetical positions of where our bins would be, our actuator would not retract and contract properly. Current draw ended up being the problem; we were successfully able to get the actuator to retract and contract.

We got the buttons to work according to documentation found here: https://docs.arduino.cc/built-in-examples/digital/Button

The final version of our code, with integrated motor, and actuator functionality is shown below: 

```cpp
void loop() {  
  delay(2000);
  int redValue = readColor(LOW, LOW); // Reading Red filter (Red light)
  int greenValue = readColor(HIGH, HIGH); // Reading Green filter (Green light)
  int blueValue = readColor(LOW, HIGH); // Reading Blue filter (Blue light)

  String color = determineColor(redValue, greenValue, blueValue);
  Serial.println(color);
  if (color == "Red") {
    myservo.write(servoPositionRed);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += RED_VALUE;
    redCount += 1;
  } else if (color == "Green") {
    myservo.write(servoPositionGreen);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += GREEN_VALUE;
    greenCount += 1;
  } else if (color == "Blue") {
    myservo.write(servoPositionBlue);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += BLUE_VALUE;
    blueCount += 1;
  } else if (color == "White") {
    myservo.write(servoPositionWhite);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += WHITE_VALUE;
    whiteCount += 1;
  } else {
 myservo.write(servoStartPosition);
  }
```


## Week of 11/13
**Objective:** 
- Complete Mock Demo, and wire contraption into Development Board, recalibrate color sensors for new lighting conditions.

**Completed:**
- Unable to flash program from ST-Link on Dev Board, need to debug with Jason
- Recalibrated Color Sensors, for TCS3200 positioned under funnel

We spent the entirety of the week recalibrating the color sensor for the new lighting conditions. We also had to adjust some of the mechanical issues with our contraption. The actuator wasn't fastened properly and was not pushing the chips out, we sanded the metal on the actuator and refastened it to fix this. However, by the end of the week we were still unable to flash our PCB with our program. We will need a buzzer-beater next week. A picture of our contraption is shown below.


![Color Spectrum](photos/IMG_5620.jpg)



## Week of 11/27
**Objective:** 
- Complete calibration, get PCB to function

**Completed:**
- Got the Blue and Green to calibrate correctly with an extremely tight tolerance
- Somehow got the PCB to work thanks to Jason, although we showed its functionality on the side for the demo so we didn't risk messing up our configuration with the nucleo board.

**Fixing the PCB:**
I was in charge of soldering the PCB, after looking at the PCB for about 30 minutes. Jason (TA) was able to notice our MCU was soldered counterclockwise of its necessary position. The ironic part is Marvin and I, looked at the PCB layout prior to placing the MCU on our PCB and we both verified it incorrectly. This is probably because we were looking at the layout when doing this instead of the datasheet.

After applying hot air to resolder the PCB we were able to finally read the PCB as a target both on the STM32Cube IDE and Arduino IDE. After two more hours of work we finished reaching full subsystem functionality on the PCB.

## Week of 12/4
**Objective:** 
- Create and Deliver Presentation Based on Feedback from Mock, Finish Final Paper

**Completed:**
- Completed presentation, went really well
- Completed paper as well

Main feedback from mock presentation was to make the presentation more visually consumable. Our final paper was done pretty easily as well, although the formatting for it was quite cumbersome.

**Overall a fun semester long project!**


