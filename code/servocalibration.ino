#include <Servo.h>
Servo myservo;

void setup() {
  myservo.attach(PB3); // Attach the servo to pin PB3
  Serial.begin(9600);  // Start serial communication at 9600 baud rate
}

void loop() {
  for (int pos = 0; pos <= 180; pos += 10) { // Goes from 0 degrees to 180 degrees in steps of 10 degrees
    myservo.write(pos);              // Tell servo to go to position in variable 'pos'
    Serial.println(pos);             // Output the position to the serial monitor

    if (pos == 40 || pos == 90 || pos == 130 || pos == 170) {
      delay(5000);  // Longer delay of 5000ms (5 seconds) at specified positions
    } else {
      delay(1000);  // Regular delay of 1000ms (1 second) at other positions
    }
  }

  for (int pos = 180; pos >= 0; pos -= 10) { // Goes from 180 degrees to 0 degrees in steps of 10 degrees
    myservo.write(pos);              // Tell servo to go to position in variable 'pos'
    Serial.println(pos);             // Output the position to the serial monitor

    if (pos == 40 || pos == 90 || pos == 130 || pos == 170) {
      delay(5000);  // Longer delay of 5000ms (5 seconds) at specified positions
    } else {
      delay(1000);  // Regular delay of 1000ms (1 second) at other positions
    }
  }
}
