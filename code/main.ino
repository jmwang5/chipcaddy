#include <LiquidCrystal.h>
#include <Servo.h>

// Pin definitions for the color sensor
#define S0 PA0
#define S1 PA1
#define S2 PA4
#define S3 PB0
#define sensorOut PC1

// Pin definitions for the LCD
const int rs = PB5, en = PB4, d4 = PC7, d5 = PB6, d6 = PA7, d7 = PA6;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Pin definitions for buttons
const int RST_BUTTON = PC0; 
const int SPLIT_BUTTON = PA8;  

// Servo setup
Servo myservo;
Servo actuator; // create a servo object named "actuator"

int servoPositionRed = 40;      // position for red
int servoPositionGreen = 165;   // position for green
int servoPositionBlue = 130;   // position for blue
int servoPositionWhite = 90;  // position for white
int servoStartPosition = 90;   // start or default position

//chip values
const float RED_VALUE = 0.25;
const float WHITE_VALUE = 0.50;
const float BLUE_VALUE = 1.00;
const float GREEN_VALUE = 2.00;

float currentPot = 0.0; // The current pot of money
int redCount = 0;
int blueCount = 0;
int greenCount = 0;
int whiteCount = 0;
int SplitButtonCount = 1;

void splitPot() {
    int splitCount = SplitButtonCount + 1;
    float portionPot = currentPot / splitCount;
    float runningTotal = 0;

    // Assuming arrays are sorted in descending order of chip value
    int chipCounts[] = {whiteCount, blueCount, greenCount, redCount};
    float chipValues[] = {WHITE_VALUE, BLUE_VALUE, GREEN_VALUE, RED_VALUE};
    int chipsToSplit[] = {0, 0, 0, 0}; // To store the number of each chip in one portion of the split

    for (int i = 0; i < 4; i++) {
        while (chipCounts[i] > 0 && (runningTotal + chipValues[i]) <= portionPot) {
            runningTotal += chipValues[i];
            chipCounts[i]--;
            chipsToSplit[i]++;
        }
        if (runningTotal >= portionPot) {
            break;
        }
    }

    // Display the split on the LCD
    lcd.clear(); 
    lcd.setCursor(0, 0);
    lcd.print("   Split x" + String(splitCount) + ":");
    lcd.setCursor(0, 1);
    lcd.print("      $" + String(portionPot));
    delay(5000); // Delay to show this information

    lcd.clear();
    lcd.setCursor(0, 0);
    lcd.print("    W:" + String(chipsToSplit[0]) + " B:" + String(chipsToSplit[1]));
    lcd.setCursor(0, 1);
    lcd.print("    G:" + String(chipsToSplit[2]) + " R:" + String(chipsToSplit[3]));

    // Additional code may be needed to handle the display of the remaining chips
}



void setup() {
  
  myservo.attach(PB3); // attaches the servo on pin PB3 to the servo object
  actuator.attach(PB10);
   actuator.writeMicroseconds(2000); // give the actuator a 2ms pulse to retract the arm (1000us = 1ms)
  delay(3000); // delay a few seconds to give the arm time to retract
  actuator.writeMicroseconds(1000); // give the actuator a 2ms pulse to retract the arm (1000us = 1ms)
  delay(3000); // delay a few seconds to give the arm time to retract
  // Setup the pins for the color sensor
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(sensorOut, INPUT);
  
  pinMode(RST_BUTTON, INPUT_PULLUP);
  pinMode(SPLIT_BUTTON, INPUT_PULLUP);
  

  // Setting frequency scaling to 20%
  digitalWrite(S0, HIGH);
  digitalWrite(S1, LOW);

  Serial.begin(9600);

  // Setup the LCD's number of columns and rows
  lcd.begin(16, 2);
//  lcd.print("   Pot: $");
//  lcd.print(currentPot, 2); // Print the pot with 2 decimal places

    lcd.print("Good luck! :)");
    // Scroll the message "Pot: $" with the value
    for (int positionCounter = 0; positionCounter < 13; positionCounter++) {
      lcd.scrollDisplayLeft();
      delay(150);
    }
    for (int positionCounter = 0; positionCounter < 29; positionCounter++) {
      lcd.scrollDisplayRight();
      delay(150);
    }
    for (int positionCounter = 0; positionCounter < 16; positionCounter++) {
      lcd.scrollDisplayLeft();
      delay(150);
    }
  
  // Move the servo to the start position on setup
  myservo.write(servoStartPosition);
  delay(1000); // wait for 1 second to let the servo reach the position
}

int splitButtonPressCount = 0; // Variable to keep track of button presses
void loop() {  
  delay(2000);
  int redValue = readColor(LOW, LOW); // Reading Red filter (Red light)
  int greenValue = readColor(HIGH, HIGH); // Reading Green filter (Green light)
  int blueValue = readColor(LOW, HIGH); // Reading Blue filter (Blue light)

  String color = determineColor(redValue, greenValue, blueValue);
  Serial.println(color);
  if (color == "Red") {
    myservo.write(servoPositionRed);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += RED_VALUE;
    redCount += 1;
  } else if (color == "Green") {
    myservo.write(servoPositionGreen);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += GREEN_VALUE;
    greenCount += 1;
  } else if (color == "Blue") {
    myservo.write(servoPositionBlue);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += BLUE_VALUE;
    blueCount += 1;
  } else if (color == "White") {
    myservo.write(servoPositionWhite);
  actuator.writeMicroseconds(2000); // 1ms pulse to extend the arm
  delay(3000); // the actuator takes >2s to extend/retract when loaded - give it plenty of time
  actuator.writeMicroseconds(1000); // 2ms pulse to retract the arm
  delay(3000);
    currentPot += WHITE_VALUE;
    whiteCount += 1;
  } else {
 myservo.write(servoStartPosition);
  }
  
if (digitalRead(RST_BUTTON) == LOW) {
  delay(50); 
  
  if (digitalRead(RST_BUTTON) == LOW) { 
    lcd.clear();
  SplitButtonCount = 0;
    // Calculate the total number of chips
    int totalChips = redCount + blueCount + greenCount + whiteCount;
    // Display the total number of chips
    lcd.print("Total Chips: ");
    lcd.print(totalChips);
    // Wait a bit for the message to be read
    delay(2000);
    currentPot = 0.00;
    redCount = 0;
    blueCount = 0;
    greenCount = 0;
    whiteCount = 0;
    splitButtonPressCount = 0;
    // Clear the screen and print the pot value
    lcd.clear();
    lcd.print("   Pot: $");
    lcd.print(currentPot, 2); 
    
    while (digitalRead(RST_BUTTON) == LOW) {
      // Waiting for button release with no action
    }
    delay(50); 
  }
}

  bool splitButtonPressed = false;


if (digitalRead(SPLIT_BUTTON) == LOW) {
    delay(50);
    if (digitalRead(SPLIT_BUTTON) == LOW) {
      splitButtonPressed = true;
        splitPot(); // Call the split function
        SplitButtonCount += 1;
        delay(5000); // Delay to view the result
    }
}

if (!splitButtonPressed) {
  lcd.clear(); // Clear the LCD to display new text
    lcd.print("   Pot: $");
    lcd.print(currentPot, 2);
    // Insert code here for scrolling this specific message if desired
  // Delay at the end of the full loop
  delay(250);
}

  delay(250); 
}

int readColor(int s2state, int s3state) {
  digitalWrite(S2, s2state);
  digitalWrite(S3, s3state);
  // The pulseIn function reads a pulse on a pin in a particular state (HIGH or LOW)
  // For the color sensor, we are interested in reading the LOW pulse width, 
  // which is the time the sensor's output pin stays LOW (in microseconds)
  // during a cycle of the sensor's output frequency.
  return pulseIn(sensorOut, LOW);
}

bool isInRange(int value, int target, int tolerance) {
  return value >= (target - tolerance) && value <= (target + tolerance);
}

String determineColor(int red, int green, int blue) {
//
  Serial.print("Red: ");
  Serial.print(red);
  Serial.print("Green: ");
  Serial.print(green);
  Serial.print("Blue: ");
  Serial.println(blue);

  // Adjust the tolerance if necessary to be more lenient with the color ranges
  const int tolerance = 3; 
  
  // Updated ranges
  if ((isInRange(red, 15, tolerance) && isInRange(green, 35, tolerance) && isInRange(blue, 25, tolerance))) {
    return "Red";
  } else if (isInRange(red, 30, tolerance) && isInRange(green, 24, tolerance) && isInRange(blue, 20, tolerance)) {
    return "Green";
  } else if (isInRange(red, 33, tolerance) && isInRange(green, 29, tolerance) && isInRange(blue, 17, tolerance)) {
    return "Blue";
  } else if (isInRange(red, 10, tolerance) && isInRange(green, 10, tolerance) && isInRange(blue, 5, tolerance)) {
    return "White";
  } else {
    return "Unknown";
  }
}
