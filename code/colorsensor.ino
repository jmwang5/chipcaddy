// TCS230 or TCS3200 pins wiring to Arduino
#define S0 4
#define S1 5
#define S2 6
#define S3 7
#define sensorOut 8

// Stores frequency read by the photodiodes
int redFrequency = 0;
int greenFrequency = 0;
int blueFrequency = 0;
// Stores the red. green and blue colors
int redColor = 0;
int greenColor = 0;
int blueColor = 0;
const int outputPin = 3; // Pin 3 is used for PWM
const int frequency = 50; // 50Hz
const int halfPeriod = 1000000 / (2 * frequency); // Half the period in microseconds

void setup() {
  // Setting the outputs
  pinMode(S0, OUTPUT);
  pinMode(S1, OUTPUT);
  pinMode(S2, OUTPUT);
  pinMode(S3, OUTPUT);
  pinMode(outputPin, OUTPUT); // Set the output pin as an OUTPUT

  // Setting the sensorOut as an input
  pinMode(sensorOut, INPUT);
 
  // Setting frequency scaling to 20%
  digitalWrite(S0,HIGH);
  digitalWrite(S1,LOW);
 
  // Begins serial communication
  Serial.begin(9600);
}

void loop() {
  analogWrite(outputPin, 128); // 128 is half of 255 (50% duty cycle)
  delayMicroseconds(halfPeriod);
  analogWrite(outputPin, 0); // 0 is the other 50% of the duty cycle
  delayMicroseconds(halfPeriod);
  // Setting RED (R) filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,LOW);
 
  // Reading the output frequency
  redFrequency = pulseIn(sensorOut, LOW);
  // Remaping the value of the RED (R) frequency from 0 to 255
  // You must replace with your own values. Here's an example:
  // redColor = map(redFrequency, 70, 120, 255,0);
  redColor = map(redFrequency, 1, 50, 255, 0);
 
  // Printing the RED (R) value
  // Serial.print("R = ");
  // Serial.print(redColor);
  // delay(1000);
 
  // Setting GREEN (G) filtered photodiodes to be read
  digitalWrite(S2,HIGH);
  digitalWrite(S3,HIGH);
 
  // Reading the output frequency
  greenFrequency = pulseIn(sensorOut, LOW);
  // Remaping the value of the GREEN (G) frequency from 0 to 255
  // You must replace with your own values. Here's an example:
  // greenColor = map(greenFrequency, 100, 199, 255, 0);
  greenColor = map(greenFrequency, 1, 50, 255, 0);
 
  // Printing the GREEN (G) value  
  // Serial.print(" G = ");
  // Serial.print(greenColor);
  // delay(1000);
 
  // Setting BLUE (B) filtered photodiodes to be read
  digitalWrite(S2,LOW);
  digitalWrite(S3,HIGH);
 
  // Reading the output frequency
  blueFrequency = pulseIn(sensorOut, LOW);
  // Remaping the value of the BLUE (B) frequency from 0 to 255
  // You must replace with your own values. Here's an example:
  // blueColor = map(blueFrequency, 38, 84, 255, 0);
  blueColor = map(blueFrequency, 10, 70, 255, 0);
 
  // Printing the BLUE (B) value
  // Serial.print(" B = ");
  // Serial.println(blueColor);
  // delay(1000);

  // Checks the current detected color and prints
  // a message in the serial monitor
// if (redColor > 180) {
//   Serial.println(" - RED detected!");
//   delay(1000);
// } else if (greenColor > 100 && blueColor < 200) {
//   Serial.println(" - GREEN detected!");
//   delay(1000);
// } else if (blueColor > 250 && blueColor < 260) {
//   Serial.println(" - BLUE detected!");
//   delay(1000);
// } else {
//   Serial.println("No color detected");
//   delay(1000);
// }



}
